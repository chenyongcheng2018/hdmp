package com.hdmp.auth.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdmp.auth.entity.SysOperationLogEntity;
import com.hdmp.auth.mapper.SysOperationLogMapper;
import org.springframework.stereotype.Component;

/**
 * @Author: Chenyc
 * @Date: 2019/1/28 16:25
 */
@Component
public class SysOperationLogService extends ServiceImpl<SysOperationLogMapper,SysOperationLogEntity> {

}