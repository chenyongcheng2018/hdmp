package com.hdmp.auth.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author BigFan
 * @create 2018/11/15 9:25 AM
 */
@Data
@TableName("sys_role")
public class SysRoleEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type =IdType.AUTO)
    private Integer dbid;
    private String code;
    private String name;
    private String remark;
    private boolean status;  //是否可用

    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value ="modify_time",fill = FieldFill.UPDATE)
    private Date modifyTime;

    @TableField(value ="create_by",fill = FieldFill.INSERT)
    private Integer createBy;

    @TableField(value="modify_by",fill = FieldFill.UPDATE)
    private Integer modifyBy;

}
