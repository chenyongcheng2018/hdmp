package com.hdmp.wework.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("wechat_gzzh")
public class GryxWechatGzzhEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键，自增长
     */
    @TableId(type = IdType.AUTO)
    private Integer dbid;

    private String accountid;
    private String appid;
    private String appsecret;
    private String mch_id;
    private String welcome;
    private Long city;
    private String grgsbh;

    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private Integer createBy;
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;
    @TableField(value = "update_by", fill = FieldFill.INSERT)
    private Integer updateBy;
    @TableField(value = "update_date", fill = FieldFill.INSERT)
    private Date updateDate;

    private String isdel;
    private String paykey;
    private String ygrkh;
    private String xgrkh;

}