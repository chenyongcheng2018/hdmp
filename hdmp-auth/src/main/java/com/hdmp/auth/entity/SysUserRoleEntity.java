package com.hdmp.auth.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.models.auth.In;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: Chenyc
 * @Date: 2019/5/25 15:05
 */
@Data
@TableName("sys_user_role")
public class SysUserRoleEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer dbid;

    private Integer userDbid;
    private Integer roleDbid;

    @TableField(value = "create_time")
    private Date createTime;

    @TableField(value ="modify_time")
    private Date modifyTime;

    @TableField(value ="create_by")
    private Integer createBy;

    @TableField(value="modify_by")
    private Integer modifyBy;
}