package com.hdmp.wework.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("wechat_keywords")
public class GryxWechatKeywordsEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键，自增长
     */
    @TableId(type = IdType.AUTO)
    private Integer dbid;

    private String accountid;
    private String keywords;
    private String content;
    private String isdel;

    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;

    @TableField(value = "update_date", fill = FieldFill.INSERT)
    private Date updateDate;
}