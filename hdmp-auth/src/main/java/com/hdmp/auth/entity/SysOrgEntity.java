package com.hdmp.auth.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.List;

/**
 * @author BigFan
 * @create 2018/11/17 9:47 PM
 */
@TableName("sys_org")
@Data
public class SysOrgEntity {

    @TableId(type = IdType.AUTO)
    private Integer dbid;
    private Integer parentDbid;
    private String name;
    private Integer orders;

    @TableField(exist = false)
    private String parentName;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    private Boolean isDel;

    @Version
    private Long version;

    @TableField(exist = false)
    private List<SysOrgEntity> children;

}
