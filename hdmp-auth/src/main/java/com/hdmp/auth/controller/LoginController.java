package com.hdmp.auth.controller;

import com.google.common.collect.Maps;
import com.hdmp.auth.entity.SysUserEntity;
import com.hdmp.auth.service.*;
import com.hdmp.tool.bean.ResultBean;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author BigFan
 * @create 2018/11/15 4:34 PM
 */
@RestController

public class LoginController {

    @Autowired
    SysUserService sysUserService;

    @Autowired
    SysRoleService roleService;

    @Autowired
    TestService testService;



    @Autowired
    SysOrgService orgService;

    @GetMapping("/login")
    public ResultBean<String> getRequest() {
        ResultBean<String> resultBean = new ResultBean<>();
        resultBean.setCode(400);
        resultBean.setData("please login first.....");
        resultBean.setMsg("系统提示：长时间未操作造成会话请求过期，请重新登录");
        return resultBean;
    }


    @PostMapping("/login")
    public ResultBean<String> postLoginRequest(HttpServletRequest request) {
        String exceptionClassName = (String) request.getAttribute("shiroLoginFailure");
        System.out.println("!!!!!!!!!!!!"+exceptionClassName);
        String error = null;
        ResultBean<String> resultBean = new ResultBean<>();
        resultBean.setCode(500);
        if (StringUtils.isEmpty(exceptionClassName)) {
            resultBean.setCode(ResultBean.SUCCESS);
            error = "已经登录成功..";
        } else if (UnknownAccountException.class.getName().equals(exceptionClassName)) {
            error = "用户名/密码错误";
        } else if (IncorrectCredentialsException.class.getName().equals(exceptionClassName)) {
            error = "用户名/密码错误";
        } else {
            error = "其他错误";
        }
        resultBean.setMsg(error);

        return resultBean;
    }

    /**
     * 登录成功后 访问的url
     *
     * @return
     */
    @GetMapping("/loginSuccess")
    public ResultBean<String> loginSuccess() {
        ResultBean<String> resultBean = new ResultBean<>("login success");
        return resultBean;

    }

    @GetMapping("/aop")
    public ResultBean<?> testAop() {
       /* sysUserService.queryByName("fzx");

        roleService.queryRoleByUserDbid(1);

        testService.testAop();*/
        SysUserEntity userEntity = new SysUserEntity();
        userEntity.setUsername("fzx");
        Map<String,String> map = Maps.newHashMap();
        map.put("sss","wds");

        List list = sysUserService.multiQueryUser(userEntity,map);
        //sysUserService.newTest("ok",2);
       // orgService.deleteTest(3l);


        return new ResultBean<>(orgService.inserOrg());
    }

}
