package com.hdmp.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.auth.entity.SysOrgEntity;

import java.util.List;

/**
 * @author BigFan
 * @create 2018/11/18 1:41 PM
 */
public interface SysOrgMapper extends BaseMapper<SysOrgEntity> {

    /**
     * 获取用户对应的组织id
     * @param dbid
     * @return
     */
    public List<Integer> queryOrgDbidByUserDbid(Integer dbid);
}
