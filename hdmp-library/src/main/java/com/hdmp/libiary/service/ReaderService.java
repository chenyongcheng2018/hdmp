package com.hdmp.libiary.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdmp.libiary.entity.ReaderEntity;
import com.hdmp.libiary.mapper.ReaderMapper;
import com.hdmp.mybatis.aspect.annotation.HdDataSource;
import com.hdmp.mybatis.constant.DataSourceEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReaderService extends ServiceImpl<ReaderMapper, ReaderEntity> {
    @Autowired
    ReaderMapper readerMapper;

    @HdDataSource(DataSourceEnum.SLAVE)
    public ReaderEntity queryByUsername(String username){
        return  readerMapper.queryByUsername(username);
    }
}