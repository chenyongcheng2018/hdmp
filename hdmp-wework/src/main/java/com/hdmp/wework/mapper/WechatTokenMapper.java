package com.hdmp.wework.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.wework.entity.GryxWechatTokenEntity;

import java.util.List;

public interface WechatTokenMapper extends BaseMapper<GryxWechatTokenEntity> {
    GryxWechatTokenEntity queryTokenByAccountid(String accountid);
    GryxWechatTokenEntity queryTokenByAppid(String appid);

    List<GryxWechatTokenEntity> queryAll();
}