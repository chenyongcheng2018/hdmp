package com.hdmp.auth.configuration;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;


/**
 * @author BigFan
 * @create 2018/11/15 4:22 PM
 */
@Configuration
public class FilterConfig {


    /**
     * 在过滤器中增加shiroFilter
     * @return
     */
    @Bean
    public FilterRegistrationBean shiroFilterRegisteration(){
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new DelegatingFilterProxy("shiroFilter"));
        registration.addInitParameter("targetFilterLifecycle","true");
        registration.setEnabled(true);
        registration.setOrder(Integer.MAX_VALUE -1);
        registration.addUrlPatterns("/*");
        return registration;
    }

}
