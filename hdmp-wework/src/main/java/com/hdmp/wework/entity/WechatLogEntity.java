package com.hdmp.wework.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: Chenyc
 * @Date: 2019/1/28 14:08
 */
@Data
@TableName("wechat_log")
public class WechatLogEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键，自增长
     */
    @TableId(type = IdType.AUTO)
    private Integer dbid;
    private String username;
    private String title;
    private String message;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")//存日期时使用
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")  //取日期时使用
    private Date moditime;
    private String flag;
    private Long zxsj;
}