package com.hdmp.auth.aspect;

/**
 * @author BigFan
 * @create 2018/11/17 2:07 PM
 */
public enum LogType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 新增
     */
    INSERT,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 删除
     */
    DELETE,

    /**
     * 查询
     */
    SELECT,

    /**
     * 导出
     */
    EXPORT,

    /**
     * 导入
     */
    IMPORT
}
