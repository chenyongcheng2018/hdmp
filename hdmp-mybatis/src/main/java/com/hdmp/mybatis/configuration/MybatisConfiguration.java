package com.hdmp.mybatis.configuration;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.baomidou.mybatisplus.extension.injector.LogicSqlInjector;
import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.hdmp.mybatis.aspect.DataSourceContextHolder;
import com.hdmp.mybatis.constant.DataSourceEnum;
import com.hdmp.mybatis.plugin.SqlTrace;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author BigFan
 * @create 2018/11/14 2:49 PM
 */
@Configuration
@MapperScan("com.hdmp.*.mapper")
@EnableTransactionManagement
@Slf4j
public class MybatisConfiguration {


    @Bean
    public SqlTrace sqlTrace(){
        return new SqlTrace();
    }

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }


    /**
     * 逻辑删除注册
     *
     * @return
     */
    @Bean
    public LogicSqlInjector sqlInject() {
        return new LogicSqlInjector();
    }

    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor() {
        return new OptimisticLockerInterceptor();
    }


    @Bean("master")
    @ConfigurationProperties("spring.datasource.druid.master")
    public DataSource masterSource() {
        log.error("master ##################$$$$$$$$4 .........................");
        return DruidDataSourceBuilder.create().build();
    }


    @Bean("slave")
    @ConfigurationProperties("spring.datasource.druid.slave")
    public DataSource slaveSource() {
        log.error("slave ##################$$$$$$$$4.........................");

        return DruidDataSourceBuilder.create().build();
    }

    @Bean("multi")
    @Primary
    @DependsOn({"master","slave"})
    public DataSourceContextHolder dataSource() {
        log.error("multi ##################$$$$$$$$4.........................");
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put(DataSourceEnum.MASTER.getValue(), masterSource());
        targetDataSources.put(DataSourceEnum.SLAVE.getValue(), slaveSource());

        return new DataSourceContextHolder(masterSource(), targetDataSources);
    }


  /*  @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource(masterSource(),slaveSource()));*/
        //sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/mapper/*/*Mapper.xml"));

       // com.baomidou.mybatisplus.core.MybatisConfiguration configuration = new com.baomidou.mybatisplus.core.MybatisConfiguration();
        //configuration.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
    /*    configuration.setJdbcTypeForNull(JdbcType.NULL);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setCacheEnabled(false);
        configuration.p
        sqlSessionFactory.setConfiguration(configuration);
        sqlSessionFactory.setPlugins(new Interceptor[]{
                paginationInterceptor(),sqlTrace()
        });*/

        //sqlSessionFactory.setGlobalConfig(globalConfiguration());
  /*      return sqlSessionFactory.getObject();
    }*/


}
