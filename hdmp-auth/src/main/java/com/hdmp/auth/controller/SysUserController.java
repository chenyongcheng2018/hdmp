package com.hdmp.auth.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.hdmp.auth.entity.SysRoleEntity;
import com.hdmp.auth.entity.SysUserEntity;
import com.hdmp.auth.service.SysUserRoleService;
import com.hdmp.auth.service.SysUserService;
import com.hdmp.auth.vo.ViewUserInfo;
import com.hdmp.tool.bean.ResultBean;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author BigFan
 * @create 2018/11/16 4:20 PM
 */
@RestController
@RequestMapping("/sysUsers")
public class SysUserController {

    @Autowired
    SysUserService userService;
    @Autowired
    SysUserRoleService userRoleService;

    @GetMapping("userinfo")
    public ViewUserInfo getUserInfo(String username){
        return userService.getUserInfo(username);
    }

    @GetMapping("")
    public ResultBean<List<SysUserEntity>> query(String username, String name) {
        Map<String, Object> condition = Maps.newHashMap();
        condition.put("username", username);
        condition.put("name", name);
        Page<SysUserEntity> page = new Page<>(0, 20);
        userService.multiQuery(condition, page);
        ResultBean<List<SysUserEntity>> resultBean = new ResultBean<>();
        resultBean.setData(page.getRecords());
        resultBean.setTotalCount(page.getTotal());
        return resultBean;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation("新增用户信息")
    public ResultBean<SysUserEntity> save(SysUserEntity userEntity) {
        userService.saveOrUpdate(userEntity);
        return new ResultBean<>(userEntity);
    }

    @GetMapping("/{dbid}")
    @ApiImplicitParam(name = "dbid", value = "用户ID", required = true, dataType = "Long")
    @ApiOperation("根据dbid获取用户信息")
    public ResultBean<SysUserEntity> load(@PathVariable Long dbid) {
        SysUserEntity userEntity = userService.getById(dbid);
        return new ResultBean<>(userEntity);
    }

    @GetMapping("/{dbid}/roles")
    @ApiImplicitParam(name = "dbid", value = "用户ID", required = true, dataType = "Long")
    @ApiOperation("根据用户主键 获取对应的角色")
    public ResultBean<List<SysRoleEntity>> queryUserRole(@PathVariable Integer dbid) {
        dbid = dbid == null ? 0 : dbid;
        List<SysRoleEntity> list = userRoleService.queryStaffRoles(dbid);
        return new ResultBean<>(list);
    }

    @GetMapping("/{dbid}/unroles")
    @ApiImplicitParam(name = "dbid", value = "用户ID", required = true, dataType = "Long")
    @ApiOperation("根据用户主键 获取未分配给用户的角色信息")
    public ResultBean<List<SysRoleEntity>> queryUserUnRole(@PathVariable Integer dbid) {
        dbid = dbid == null ? 0 : dbid;
        List<SysRoleEntity> list = userRoleService.queryUnStaffRoles(dbid);
        return new ResultBean<>(list);
    }

    @GetMapping("/validate/username")
    @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String")
    @ApiOperation("验证用户名是否已经存在")
    public ResultBean<?> validateUserName(String username) {
        SysUserEntity sysUserEntity = userService.queryByName(username);
        return new ResultBean<>(sysUserEntity == null? true : false);
    }

    @GetMapping("/validate/password")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "Long"),
            @ApiImplicitParam(name = "password", value = "用户密码", required = true, dataType = "String"),
    })
    @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String")
    @ApiOperation("验证用户密码是否正确")
    public ResultBean<?> validateUserPassword(Long userid,String password) {

        return new ResultBean<>(userService.validPassword(userid,password));
    }

    @PostMapping("/modify/password")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userid", value = "用户id", required = true, dataType = "Long"),
            @ApiImplicitParam(name = "newpwd", value = "用户新密码", required = true, dataType = "String"),
    })
    @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String")
    @ApiOperation("修改新密码")
    public ResultBean<?> modifyPassword(Long userid,String newpwd) {
        userService.saveNewPassword(userid,newpwd);
        return new ResultBean<>();
    }

}
