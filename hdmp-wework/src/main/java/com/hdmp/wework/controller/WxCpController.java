package com.hdmp.wework.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/wework/{appId}")
@Api(description = "微信公众号统一接口")
@CrossOrigin
@Slf4j
public class WxCpController {
    @ApiOperation(value = "微信公众号验证GET请求：开发者模式启动时验证")
    @GetMapping(produces = "text/plain;charset=utf-8")
    public String checkWechat(@PathVariable String appId,
                              @RequestParam(name = "signature", required = false) String signature,
                              @RequestParam(name = "timestamp", required = false) String timestamp,
                              @RequestParam(name = "nonce", required = false) String nonce,
                              @RequestParam(name = "echostr", required = false) String echostr){
        log.info("Received Auth info from WeChat Server: [{}, {}, {}, {}]", signature, timestamp, nonce, echostr);

        return "illegal request.";
    }
}