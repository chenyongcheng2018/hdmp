package com.hdmp.wework.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("wechat_gzyh")
public class GryxWechatGzyhEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键，自增长
     */
    @TableId(type = IdType.AUTO)
    private Integer dbid;
    private String appid;
    private String openid;
    private Boolean subscribe;
    private String sex;
    private Date subscribetime;
    private String nickname;
    private String country;
    private String province;
    private String city;
    private String headimgurl;
    private String isdel;

    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;

    @TableField(value = "update_date", fill = FieldFill.INSERT)
    private Date updateDate;

}