package com.hdmp.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.auth.entity.SysPermissionEntity;
import com.hdmp.auth.entity.SysRolePermissionEntity;

import java.util.List;

/**
 * @Author: Chenyc
 * @Date: 2019/5/25 15:26
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermissionEntity> {
    public List<SysPermissionEntity> queryPermissionByRole(List<Integer> roleDbids);
}