package com.hdmp.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.mybatis.entity.TUser;

/**
 * @author BigFan
 * @create 2018/11/14 3:09 PM
 */
public interface TUserMapper extends BaseMapper<TUser> {
}
