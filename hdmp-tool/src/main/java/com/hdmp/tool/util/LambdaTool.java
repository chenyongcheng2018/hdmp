package com.hdmp.tool.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author BigFan
 * @create 2018/11/16 1:22 PM
 */
public class LambdaTool {

    /**
     * 根据指定的属性去重
     * @param keyExtractor
     * @param <T>
     * @return
     */
    public   <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
