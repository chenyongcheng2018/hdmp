package com.hdmp.libiary.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hdmp.libiary.entity.BooksEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BooksMapper extends BaseMapper<BooksEntity> {
    List<BooksEntity> queryByTm(Page page, @Param("tm") String tm);
    BooksEntity queryByZjm(@Param("zjm") String zjm);
}