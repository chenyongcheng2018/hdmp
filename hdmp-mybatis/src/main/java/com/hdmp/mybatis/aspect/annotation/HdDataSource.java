package com.hdmp.mybatis.aspect.annotation;

import com.hdmp.mybatis.constant.DataSourceEnum;

import java.lang.annotation.*;

/**
 * @author BigFan
 * @create 2018/11/20 1:00 PM
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HdDataSource {
    DataSourceEnum value() default DataSourceEnum.MASTER;
}
