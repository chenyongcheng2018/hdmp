package com.hdmp.auth.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.hdmp.auth.shiro.ShiroUser;
import com.hdmp.mybatis.constant.DatabaseFieldConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.security.SecurityUtil;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author BigFan
 * @create 2018/11/16 3:24 PM
 */
@Slf4j
@Component
public class HdmpMetaObjectHandler implements MetaObjectHandler {


    /**
     * 新增数据时候
     *
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {


        if (metaObject.hasSetter(DatabaseFieldConstant.USERNAME) && getFieldValByName(DatabaseFieldConstant.USERNAME, metaObject) == null) {

            setFieldValByName(DatabaseFieldConstant.USERNAME, getUsername(), metaObject);
        }

        if (metaObject.hasSetter(DatabaseFieldConstant.CREATE_BY) && getFieldValByName(DatabaseFieldConstant.CREATE_BY, metaObject) == null) {
            Object object = getFieldValByName(DatabaseFieldConstant.CREATE_BY, metaObject);

            setFieldValByName(DatabaseFieldConstant.CREATE_BY, getUserDbid(), metaObject);
        }

        if (metaObject.hasSetter(DatabaseFieldConstant.CREATE_TIME)) {
            setFieldValByName(DatabaseFieldConstant.CREATE_TIME, new Date(), metaObject);
        }

        //Object modifyBy = getFieldValByName(DatabaseFieldConstant.MODIFY_BY, metaObject);
        if (metaObject.hasSetter(DatabaseFieldConstant.MODIFY_BY) && getFieldValByName(DatabaseFieldConstant.MODIFY_BY, metaObject) == null) {
            setFieldValByName(DatabaseFieldConstant.MODIFY_BY, getUserDbid(), metaObject);
        }

        Object modifyTime = getFieldValByName(DatabaseFieldConstant.MODIFY_TIME, metaObject);
        if (metaObject.hasSetter(DatabaseFieldConstant.MODIFY_TIME)) {
            setFieldValByName(DatabaseFieldConstant.MODIFY_TIME, new Date(), metaObject);
        }

    }

    /**
     * 更新数据时候
     *
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {


        //Object modifyBy = getFieldValByName(DatabaseFieldConstant.MODIFY_BY, metaObject);
        if (metaObject.hasSetter(DatabaseFieldConstant.MODIFY_BY) && metaObject.hasSetter(DatabaseFieldConstant.MODIFY_BY)) {
            setFieldValByName(DatabaseFieldConstant.MODIFY_BY, getUserDbid(), metaObject);
        }

        Object modifyTime = getFieldValByName(DatabaseFieldConstant.MODIFY_TIME, metaObject);
        if (metaObject.hasSetter(DatabaseFieldConstant.MODIFY_TIME) && metaObject.hasSetter(DatabaseFieldConstant.MODIFY_TIME)) {
            setFieldValByName(DatabaseFieldConstant.MODIFY_TIME, new Date(), metaObject);
        }

    }


    public String getUsername() {
        return getShiroUser() == null ? "未登录用户" : getShiroUser().getUsername();
    }

    /**
     * 获取用户id
     *
     * @return
     */
    public Integer getUserDbid() {
        return getShiroUser() == null ? 0 : getShiroUser().getId();
    }


    public ShiroUser getShiroUser() {
        if (SecurityUtils.getSubject() == null) {
            return null;
        }
        Subject subject = SecurityUtils.getSubject();
        ShiroUser user = (ShiroUser) subject.getPrincipal();
        return user;
    }
}
