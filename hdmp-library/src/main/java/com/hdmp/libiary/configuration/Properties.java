package com.hdmp.libiary.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: Chenyc
 * @Date: 2019/6/9 10:12
 */
@Data
@Configuration
public class Properties {

    @Value("${system.uploadDir}")
    private String uploadDir;
}