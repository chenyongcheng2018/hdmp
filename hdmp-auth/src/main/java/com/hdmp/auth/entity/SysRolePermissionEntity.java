package com.hdmp.auth.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author BigFan
 * @create 2018/11/15 9:57 AM
 */
@Data
@TableName("sys_role_permission")
public class SysRolePermissionEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer dbid;

    private Integer roleDbid;
    private Integer permissionDbid;

    private String operation;

    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value ="modify_time",fill = FieldFill.UPDATE)
    private Date modifyTime;

    @TableField(value ="create_by",fill = FieldFill.INSERT)
    private Integer createBy;

    @TableField(value="modify_by",fill = FieldFill.UPDATE)
    private Integer modifyBy;

    private String formula;

}
