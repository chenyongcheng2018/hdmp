package com.hdmp.auth.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author BigFan
 * @create 2018/11/18 1:40 PM
 */
@TableName("sys_role_org")
@Data
public class SysRoleOrgEntity {
    private Long dbid;


    private Long roleDbid;
    private Long orgDbid;
}
