package com.hdmp.auth.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.springframework.transaction.annotation.Transactional;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author BigFan
 * @create 2018/11/14 8:58 PM
 */
@TableName("sys_user")
@Data
public class SysUserEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer dbid;

    private Integer orgDbid;

    private String username;
    private String password;
    private String name;

    private String email;

    private String mobile;

    private String salt;

    private Boolean superUser;

    private String dztm;

    /**
     * 状态 (0:禁用  1: 正常)
     */
    private Integer status;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "modify_time", fill = FieldFill.UPDATE)
    private Date modifyTime;

    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private Integer createBy;

    @TableField(value = "modify_by", fill = FieldFill.UPDATE)
    private Integer modifyBy;

    @TableField(exist = false)
    private List<Integer> addRoles;
    @TableField(exist = false)
    private List<Integer> delRoles;

    private String flag;

    public String getCredentialsSalt() {
        return username + salt;
    }
}
