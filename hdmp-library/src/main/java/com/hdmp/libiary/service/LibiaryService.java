package com.hdmp.libiary.service;

import com.google.common.base.Strings;
import com.hdmp.auth.aspect.LogType;
import com.hdmp.auth.aspect.SysLog;
import com.hdmp.auth.entity.SysUserEntity;
import com.hdmp.auth.service.SysUserService;
import com.hdmp.libiary.entity.BooksEntity;
import com.hdmp.libiary.entity.CurrencyEntity;
import com.hdmp.libiary.entity.ReaderEntity;
import com.hdmp.tool.bean.ResultBean;
import com.hdmp.tool.exception.BaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class LibiaryService{
    @Autowired
    SysUserService userService;
    @Autowired
    ReaderService readerService;
    @Autowired
    BooksService booksService;
    @Autowired
    CurrencyService currencyService;


    public ResultBean bindCode(String username, String password, String openid){
        ResultBean bean = new ResultBean<>();
        String error = "";
        //todo 先验证是否已经绑定，再验证业务用户表中的用户名和密码是否一致，然后绑定值
        List<SysUserEntity> list = userService.queryByDztm(username);
        if(list != null && list.size() > 0){
            throw new BaseException("用户已绑定");
        }
        SysUserEntity userEntity = userService.queryByName(openid);
        if(userEntity == null){
            throw new BaseException("连接微信服务异常，请稍后再试");
        }
        if (!Strings.isNullOrEmpty(username) && !Strings.isNullOrEmpty(password)) {
            System.out.println("username:" + username + ",password:" + password + "have not login,需要验证一下啊");
            ReaderEntity readerEntity = readerService.queryByUsername(username);
            if (readerEntity == null) {
                error = "用户名不存在";
            } else {
                if("注销".equals(readerEntity.getGszx())){
                    error = "用户已注销";
                }else if (password.equals(readerEntity.getMm())) {//验证通过后可以绑定
                    userEntity.setDztm(readerEntity.getDztm());
                    userEntity.setName(readerEntity.getXm());
                    userService.saveOrUpdate(userEntity);
                    error = "success";
                } else {
                    error = "密码错误";
                }
            }
        } else {
            error = "用户名和密码不能为空";
        }

        bean.setMsg(error);
        bean.setData(userEntity);

        return bean;
    }

    @SysLog(title = "解除绑定卡号",logType = LogType.UPDATE)
    public ResultBean rebindCode(String openid){
        ResultBean bean = new ResultBean<>();
        SysUserEntity userEntity = userService.queryByName(openid);
        userEntity.setDztm("");
        userService.saveOrUpdate(userEntity);
        bean.setData(userEntity);
        bean.setMsg("解除绑定成功");
        return bean;
    }

    //关联对照表查询是否已经关联用户，如果关联则将关联用户带出
    public SysUserEntity getUser(String username){
        SysUserEntity userEntity = userService.queryByName(username);
        return userEntity;
    }

    public List<CurrencyEntity> queryCurrency(String dztm){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        List<CurrencyEntity> list = currencyService.queryByDztm(dztm);
        ReaderEntity readerEntity = readerService.queryByUsername(dztm);
        System.out.println(list.toString());
        for (CurrencyEntity entity : list) {
            if("0".equals(entity.getXjcs())){
                entity.setSfyxxj(true);
            }else{
                entity.setSfyxxj(false);
            }
            entity.setQfk(readerEntity.getQfk());
            BooksEntity booksEntity = booksService.queryByZjm(entity.getZjm());
            entity.setTm(booksEntity.getTm());
            entity.setSsh(booksEntity.getSsh());
            entity.setZrz(booksEntity.getZrz());
            entity.setBzbm(booksEntity.getBzbm());
            entity.setCbz(booksEntity.getCbz());
            entity.setCbd(booksEntity.getCbd());
            entity.setCbrq(booksEntity.getCbrq());
            entity.setHsts(daysBetween(sdf.format(new Date()), entity.getYghsj()));
        }
        return list;
    }
    //计算日期差

    /**
     *
     * @param hssj  还书日期
     * @param dqsj  当前日期
     * @return
     */
    public int daysBetween(String dqsj, String hssj){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date hsrq; Date dqrq;
        try{
            dqrq=sdf.parse(dqsj);
            hsrq=sdf.parse(hssj);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(dqrq);
        long time1 = cal.getTimeInMillis();
        cal.setTime(hsrq);
        long time2 = cal.getTimeInMillis();
        long between_days=(time2-time1)/(1000*3600*24);

        return Integer.parseInt(String.valueOf(between_days));
    }
    //13标准码转13位ISBN码
    public String to13isbn(String isbn){
        String newstr=isbn.substring(3);
        String c=toisbn2(newstr);
        return isbn.substring(0,3)+"-"+c;
    }
    //13标准码转10位ISBN码
    public String toisbn(String isbn){
        if(isbn.indexOf("-")!=-1||isbn.length()!=13)return isbn; //如果包含-或不是13位,则返回
        String newstr=isbn.substring(3, isbn.length()-1);
        int sum=0;
        for(int i=0,j=10;i<newstr.length();i++,j--){
            int a=Integer.parseInt(newstr.substring(i,i+1));
            sum=sum+a*j;
        }
        int b=11-sum%11; //得到校验位
        String temp = String.valueOf(b);
        if(b==10){
            temp = "X";
        }
        String c=toisbn2(newstr+temp);
        return c;
    }

    public String toisbn2(String srcISBN){
        String retISBN = srcISBN;
        if(srcISBN.charAt(1) != '-'){
            switch(srcISBN.charAt(1))
            {
                case '0':
                    retISBN = srcISBN.charAt(0)
                            + "-"
                            + srcISBN.charAt(1)
                            + srcISBN.charAt(2)
                            + "-"
                            + srcISBN.charAt(3)
                            + srcISBN.charAt(4)
                            + srcISBN.charAt(5)
                            + srcISBN.charAt(6)
                            + srcISBN.charAt(7)
                            + srcISBN.charAt(8)
                            + "-"
                            + srcISBN.charAt(9);
                    break;

                case '1':
                case '2':
                case '3':
                    retISBN= srcISBN.charAt(0)
                            + "-"
                            + srcISBN.charAt(1)
                            + srcISBN.charAt(2)
                            + srcISBN.charAt(3)
                            + "-"
                            + srcISBN.charAt(4)
                            + srcISBN.charAt(5)
                            + srcISBN.charAt(6)
                            + srcISBN.charAt(7)
                            + srcISBN.charAt(8)
                            + "-"
                            + srcISBN.charAt(9);
                    break;

                case '5':
                    retISBN= srcISBN.charAt(0)
                            + "-"
                            + srcISBN.charAt(1)
                            + srcISBN.charAt(2)
                            + srcISBN.charAt(3)
                            + srcISBN.charAt(4)
                            + "-"
                            + srcISBN.charAt(5)
                            + srcISBN.charAt(6)
                            + srcISBN.charAt(7)
                            + srcISBN.charAt(8)
                            + "-"
                            + srcISBN.charAt(9);
                    break;
                case '4':
                case '8':
                    retISBN = srcISBN.charAt(0)
                            + "-"
                            + srcISBN.charAt(1)
                            + srcISBN.charAt(2)
                            + srcISBN.charAt(3)
                            + srcISBN.charAt(4)
                            + srcISBN.charAt(5)
                            + "-"
                            + srcISBN.charAt(6)
                            + srcISBN.charAt(7)
                            + srcISBN.charAt(8)
                            + "-"
                            + srcISBN.charAt(9);
                    break;
                case '7'://add Wang .CD or DVD more
                    retISBN = srcISBN.charAt(0)
                            + "-"
                            + srcISBN.charAt(1)
                            + srcISBN.charAt(2)
                            + srcISBN.charAt(3)
                            + srcISBN.charAt(4)
                            + "-"
                            + srcISBN.charAt(5)
                            + srcISBN.charAt(6)
                            + srcISBN.charAt(7)
                            + srcISBN.charAt(8)
                            + "-"
                            + srcISBN.charAt(9);
                    break;
                default:
                    retISBN = srcISBN;
                    break;
            }
        }
        return retISBN;
    }
}