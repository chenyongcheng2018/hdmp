package com.hdmp.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.auth.entity.SysOperationLogEntity;

/**
 * @author BigFan
 * @create 2018/11/17 2:36 PM
 */
public interface SysOperationLogMapper extends BaseMapper<SysOperationLogEntity>{
}
