package com.hdmp.tool.util;

/**
 * @author BigFan
 * @create 2018/11/17 8:38 PM
 */
public class BeanProperty {
    private final String name;
    private final Class<?> type;

    public BeanProperty(String name, Class<?> type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Class<?> getType() {
        return type;
    }
}
