package com.hdmp.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author BigFan
 * @create 2018/11/14 3:00 PM
 */
@TableName("t_user")
@Data
public class TUser {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private String bz;
}
