package com.hdmp.wework.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.wework.entity.GryxWechatGzyhEntity;
import org.apache.ibatis.annotations.Param;

public interface WechatGzyhMapper extends BaseMapper<GryxWechatGzyhEntity> {
    GryxWechatGzyhEntity queryByOpenIdAndIsdel(@Param("openid") String openid);
}