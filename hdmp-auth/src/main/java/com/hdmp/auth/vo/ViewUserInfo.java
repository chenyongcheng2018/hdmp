package com.hdmp.auth.vo;

import com.hdmp.auth.entity.SysPermissionEntity;
import com.hdmp.auth.entity.SysRoleEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @Author: Chenyc
 * @Date: 2019/5/25 14:54
 */
@Data
public class ViewUserInfo {
    private Long dbid;
    private String username;
    private String name;
    private String jobNo;
    private Long orgDbid;
    private Boolean superUser;
    private Set<PermissionInfo> menues;
    private List<PermissionInfo> permissionInfos;
    private List<SysRoleEntity> roleInfos;
    private List<String> roles;  //用户角色列表
    private Set<Long> orgs;  // 用户角色组织对应表的 组织id
    private Set<Long> childrenOrgs;  // 用户所在组织的下属组织id
    private Date createTime;
    private Date modifyTime;
    private Date lastSignDate;

}