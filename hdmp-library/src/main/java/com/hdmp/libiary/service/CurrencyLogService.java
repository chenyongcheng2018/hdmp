package com.hdmp.libiary.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdmp.libiary.entity.CurrencyLogEntity;
import com.hdmp.libiary.mapper.CurrencyLogMapper;
import com.hdmp.mybatis.aspect.annotation.HdDataSource;
import com.hdmp.mybatis.constant.DataSourceEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CurrencyLogService extends ServiceImpl<CurrencyLogMapper, CurrencyLogEntity> {
    @Autowired
    CurrencyLogMapper logMapper;

    @HdDataSource(DataSourceEnum.SLAVE)
    public Page queryByDztm(Page<CurrencyLogEntity> page , String dztm, String begtime, String endtime){
        begtime = begtime + " 00:00:00";
        endtime = endtime + " 23:59:59";
        System.out.println(begtime+"::"+endtime);
        List<CurrencyLogEntity> list = logMapper.queryByDztm(page, dztm, begtime, endtime);
        System.out.println(list.size());
        page.setRecords(list);
        System.out.println(page.getRecords().size());
        int total = logMapper.queryTotal(dztm, begtime, endtime);
        System.out.println(page.getTotal());
        page.setTotal(total);
        return page;
    }

    @HdDataSource(DataSourceEnum.SLAVE)
    public List<CurrencyLogEntity> queryByDztmAndZjm(String dztm, String zjm, String begtime, String endtime){
        begtime = begtime + " 00:00:00";
        endtime = endtime + " 23:59:59";
        System.out.println(begtime+"::"+endtime);
        List<CurrencyLogEntity> list = logMapper.queryByDztmAndZjm(dztm, zjm, begtime, endtime);
        for(CurrencyLogEntity logEntity : list){
            if(logEntity.getPfk() == 0D){
                logEntity.setPfk(null);
            }
            if(logEntity.getCqts() == 0D){
                logEntity.setCqts(0);
            }
            String str = ("J".equals(logEntity.getCzlx()))?"借书时间":("H".equals(logEntity.getCzlx()))?"还书时间":"续借时间";
            logEntity.setBz(str);
        }
        return list;
    }
}