package com.hdmp.wework.controller;

import com.hdmp.wework.service.WeixinTokenService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
@Api(description = "")
@CrossOrigin
@Slf4j
public class TestController {
    @Autowired
    WeixinTokenService tokenService;

    @ResponseBody
    @RequestMapping(value = "/{appId}" ,method = RequestMethod.GET)
    public String getOpenid(@PathVariable String appId, @RequestParam(name = "code", required = false) String code){
        System.out.println("appid=="+appId);
        System.out.println("code=="+code);
        String str = tokenService.getOpenId(appId, code);
        System.out.println(str);
        return str;
    }
}