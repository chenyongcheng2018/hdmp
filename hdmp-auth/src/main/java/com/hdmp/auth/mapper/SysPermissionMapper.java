package com.hdmp.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.auth.entity.SysPermissionEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author BigFan
 * @create 2018/11/16 12:38 PM
 */
public interface SysPermissionMapper extends BaseMapper<SysPermissionEntity> {

    public List<SysPermissionEntity> queryPermissionByUsername(String username);
    public List<SysPermissionEntity> queryPermissionByType(String type);
    public List<SysPermissionEntity> queryByParentIdAndType(@Param("parentId") Integer parentId, @Param("type") String type);
    public List<SysPermissionEntity> queryMenuUserDbid(Integer dbid);
}
