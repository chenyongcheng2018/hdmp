package com.hdmp.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.auth.entity.SysUserRoleEntity;

import java.util.Map;

/**
 * @Author: Chenyc
 * @Date: 2019/5/25 15:09
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRoleEntity> {
    void deleteByUserIDAndRoleId(Map<String, Object> params);
}