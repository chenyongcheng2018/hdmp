package com.hdmp.wework.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("wechat_token")
public class GryxWechatTokenEntity  implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键，自增长
     */
    @TableId(type = IdType.AUTO)
    private Integer dbid;
    private String appid;
    private String accountid;
    private String token;
    @TableField(value = "time_version", fill = FieldFill.INSERT)
    private Long timeVersion;
    private String ticket;
}