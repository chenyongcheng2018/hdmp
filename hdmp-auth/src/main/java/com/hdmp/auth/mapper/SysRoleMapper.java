package com.hdmp.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hdmp.auth.entity.SysPermissionEntity;
import com.hdmp.auth.entity.SysRoleEntity;
import com.hdmp.auth.entity.SysUserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author BigFan
 * @create 2018/11/16 12:38 PM
 */
public interface SysRoleMapper extends BaseMapper<SysRoleEntity> {

    List<SysRoleEntity> query(Page page, @Param("params") Map<String, Object> map);
    List<SysRoleEntity> queryStaffRoles(Integer dbid);
    List<SysRoleEntity> queryUnStaffRoles(Integer dbid);
    List<SysUserEntity> queryUserByRoleId(Integer roleId);

    List<SysPermissionEntity> queryPermissionByRoleId(@Param("roleId") Integer roleId, @Param("type") String type);
    List<SysRoleEntity> queryRoleByUserDbid(Integer dbid);
}
