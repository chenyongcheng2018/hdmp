package com.hdmp.wework.handler;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpMessageHandler;

@Slf4j
public abstract class AbstractHandler implements WxMpMessageHandler {

}