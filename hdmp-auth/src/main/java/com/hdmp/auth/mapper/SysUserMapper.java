package com.hdmp.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hdmp.auth.entity.SysUserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author BigFan
 * @create 2018/11/15 10:26 AM
 */
public interface SysUserMapper extends BaseMapper<SysUserEntity> {
    public List<SysUserEntity> query(Page page, @Param("params") Map<String, Object> map) ;
}

