package com.hdmp.auth.vo;

import lombok.Data;

import java.util.Set;

/**
 * @Author: Chenyc
 * @Date: 2019/5/25 16:11
 */
@Data
public class PermissionInfo implements Comparable<PermissionInfo>  {
    private Integer dbid;
    private Integer parentId;
    private String name;
    private String url;
    private String type; // 00 表示模块显示, 01 标识操作权限
    private String icon;
    /**
     * 权限编码
     */
    private String code;
    private String method;
    private String component;
    private Integer orders;
    private Boolean isDisplay;

    private Boolean open;
    private String perm;
    Set<PermissionInfo> permissionInfos;
    @Override
    public int compareTo(PermissionInfo o) {
        return this.orders-o.orders;
    }
}