package com.hdmp.auth.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author BigFan
 * @create 2018/11/17 2:00 PM
 */
@TableName("sys_operation_log")
@Data
public class SysOperationLogEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**成功*/
    public final static  String OPERATION_LOG_SUCCESS="1";
    /**失败*/
    public final static  String OPERATION_LOG_FAIL="0";


    @TableId(type = IdType.AUTO)
    private Long dbid;
    @TableField(value = "username",fill = FieldFill.INSERT)
    private String username;   //操作用户名

    private String title;
    private String content;
    private String operateType;
    private String requestUri;
    private String operateIp;
    private String method;
    private String params;
    private String exception;
    private String status;  //是否执行成功
    private Date beginTime;   //操作开始时间
    private Long executeTime;  //操作时长

    private String requestParams;  //reuest的所有key及value

    @TableField(value="create_by",fill = FieldFill.INSERT)
    private Integer createBy;

    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;


}
