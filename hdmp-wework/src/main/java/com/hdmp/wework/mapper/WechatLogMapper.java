package com.hdmp.wework.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.wework.entity.WechatLogEntity;

/**
 * @Author: Chenyc
 * @Date: 2019/1/28 14:10
 */
public interface WechatLogMapper extends BaseMapper<WechatLogEntity> {

}