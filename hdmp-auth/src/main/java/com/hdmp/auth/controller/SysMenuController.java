package com.hdmp.auth.controller;

import com.hdmp.auth.entity.SysPermissionEntity;
import com.hdmp.auth.service.SysPermissionService;
import com.hdmp.tool.bean.ResultBean;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Chenyc
 * @Date: 2019/5/26 12:59
 */
@RestController
@RequestMapping("/menu")
public class SysMenuController {
    @Autowired
    SysPermissionService menuService;

    @GetMapping(value = "/findAll")
    @ApiOperation(value = "查询所有菜单数据", notes = "查询所有菜单数据")
    public ResultBean<List<SysPermissionEntity>> findAllByType() {
        ResultBean<List<SysPermissionEntity>> resultBean = new ResultBean<>(menuService.findByType("00"));
        return resultBean;
    }

    @GetMapping(value = "/findByParentId")
    @ApiOperation(value = "根据用户ID查询下层数据", notes = "根据用户ID查询下层数据")
    public ResultBean<List<SysPermissionEntity>> findByParentId(String parentId, String type) {
        ResultBean<List<SysPermissionEntity>> resultBean = new ResultBean<>(menuService.findByParentId(parentId, type));
        return resultBean;
    }

    @PostMapping(value = "/save")
    @ApiOperation(value = "保存菜单数据", notes = "新增加菜单数据")
    public ResultBean<SysPermissionEntity> saveMenu(SysPermissionEntity entity) {
        menuService.saveOrUpdate(entity);
        return new ResultBean<>(entity);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "删除菜单数据", notes = "删除菜单数据")
    public ResultBean<Boolean> deleteMenu(@ApiParam(value = "菜单ID") @PathVariable String id) {
        ResultBean<Boolean> resultBean = new ResultBean<>(menuService.removeById(id));
        return resultBean;
    }

    /*@GetMapping(value = "/findExpression/{id}")
    @ApiOperation(value = "根据权限ID获取表达式数据", notes = "根据权限ID获取表达式数据")
    public ResultBean<List<SysPermissionExpressionEntity>> findAllByPermissionId(@ApiParam(value = "权限ID") @PathVariable String id) {
        ResultBean<List<SysPermissionExpressionEntity>> resultBean = new ResultBean<>(menuService.findByPermissionId(id));
        return resultBean;
    }*/

    /*@PostMapping(value = "/saveExpressions")
    public ResultBean<Boolean> saveExpressions(@RequestBody  SysPermissionExpressionEntity[] entities){
        List<SysPermissionExpressionEntity> entitiesList = Arrays.asList(entities);
        return new ResultBean<>(sysPermissionExpressionService.saveOrUpdateBatch(entitiesList));
    }*/

    @DeleteMapping(value = "/deletePermission/{id}")
    @ApiOperation(value = "删除权限数据", notes = "删除权限数据，同时关联删除表达式表及角色权限关联表相关数据")
    public ResultBean<Boolean> deletePermission(@ApiParam(value = "权限ID") @PathVariable String id) {
        ResultBean<Boolean> resultBean = new ResultBean<>(menuService.deletePermission(id));
        return resultBean;
    }

    @RequestMapping(value = "/treeModules/{parentId}", method = RequestMethod.GET)
    public ResultBean<?> getModuleTree(@PathVariable Integer parentId) throws Exception {
        parentId = parentId == null ? 0 : parentId;
        ResultBean resultBean = new ResultBean();
        List<SysPermissionEntity> list = menuService.getAllChildrenMenu(parentId);
        resultBean.setData(list);
        return resultBean;
    }
}