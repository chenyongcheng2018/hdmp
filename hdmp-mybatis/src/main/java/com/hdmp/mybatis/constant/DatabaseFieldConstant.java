package com.hdmp.mybatis.constant;

/**
 * @author BigFan
 * @create 2018/11/16 3:32 PM
 * 数据库通用字段常量
 */

public interface DatabaseFieldConstant {



    /**
     * 用户名
     */
    public static final String USERNAME = "username";
    /**
     * 删除标记
     */
    public static final String DEL_FLAG = "idDel";
    /**
     * 创建人
     */
    public static final String CREATE_BY = "createBy";
    /**
     * 创建时间
     */
    public static final String CREATE_TIME = "createTime";
    /**
     * 修改人
     */
    public static final String MODIFY_BY = "modifyBy";
    /**
     * 修改时间
     */
    public static final String MODIFY_TIME = "modifyTime";

    /**
     * 数据权限过滤sql
     */
    public static final String DATA_FILTER_SQL = "dataFilterSql";




}
