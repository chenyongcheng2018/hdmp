package com.hdmp.wework.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.wework.entity.GryxWechatGzzhEntity;

public interface WechatGzzhMapper extends BaseMapper<GryxWechatGzzhEntity>{
    GryxWechatGzzhEntity queryGzzhByAccountid(String accountid);
}