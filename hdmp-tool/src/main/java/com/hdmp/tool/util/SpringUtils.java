package com.hdmp.tool.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;

/**
 * @author BigFan
 * @create 2018/11/14 4:17 PM
 */
public class SpringUtils implements ApplicationContextAware{
    private static ApplicationContext context;
    private SpringUtils(){

    }
    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        SpringUtils.context = context;
    }

    public static <T> T getBean(Class<T> clazz){
        if (clazz == null) return null;
        return context.getBean(clazz);
    }

    public static <T> T getBean(String beanName, Class<T> clazz) {
        if (null == beanName || "".equals(beanName.trim())) {
            return null;
        }
        if (clazz == null) return null;
        return (T) context.getBean(beanName, clazz);
    }

    public static ApplicationContext getContext(){
        if (context == null) return null;
        return context;
    }

    //发布spring 事件
    public static void publishEvent(ApplicationEvent event) {
        if (context == null) return;
        context.publishEvent(event);
    }
}
