package com.hdmp.libiary.entity;
import lombok.Data;

import java.util.Date;

@Data
public class ReaderEntity {
    private String jszh;//借书证号;
    private String dztm;//读者条码;
    private String mm;//密码;
    private String xm;//x;
    private String xb;//性别;
    private String sfzh;//身份证号;
    private String dzjb;//读者级别;
    private String jbdm;//级别代码;
    private String dwdm;//单位代码;
    private String dw;//单位;
    private String dh;//电话;
    private String lxdz;//联系地址;
    private int kwj;//可外借;
    private int ywj;//已外借;
    private Date ksrq;//发证日期;
    private Date jsrq;//失效日期;
    private String gszx;//挂失注销; 判断用户是否激活
    private Date gsrq;//挂失日期;
    private String qfk;//欠罚款;
    private String qpk;//欠赔款;


}