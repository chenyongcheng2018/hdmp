package com.hdmp.libiary.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdmp.libiary.entity.BooksEntity;
import com.hdmp.libiary.mapper.BooksMapper;
import com.hdmp.mybatis.aspect.annotation.HdDataSource;
import com.hdmp.mybatis.constant.DataSourceEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BooksService extends ServiceImpl<BooksMapper, BooksEntity> {
    @Autowired
    BooksMapper booksMapper;

    @HdDataSource(DataSourceEnum.SLAVE)
    public Page queryByTm(Page<BooksEntity> page ,String tm){
        List<BooksEntity> list = booksMapper.queryByTm(page, tm);
        page.setRecords(list);
        return page;
    }

    @HdDataSource(DataSourceEnum.SLAVE)
    public BooksEntity queryByZjm(String zjm){
        return booksMapper.queryByZjm(zjm);
    }
}