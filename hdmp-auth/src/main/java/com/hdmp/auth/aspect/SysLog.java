package com.hdmp.auth.aspect;

import java.lang.annotation.*;

/**
 * @author BigFan
 * @create 2018/11/17 2:08 PM
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {


    /**
     * 请求标题
     * @return
     */
    String title() default "";

    /**
     * 请求类型
     * @return
     */
    LogType logType() default LogType.OTHER;


    /**
     * 是否需要将request请求的所有参数都但因出来
     * @return
     */
    boolean requestParam() default false;

}
