package com.hdmp.libiary.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class CurrencyLogEntity {
    private String czlx;    //操作类型
    private String zjm;        //主键码
    private String txm;     //条形码
    private String dlh;     //登录号
    private String dztm;    //读者条码
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date clsj;      //处理时间
    private Double pfk;     //赔罚款
    private int cqts;       //超期天数
    private Double tp;      //退赔
    private String bz;      //备注
    private String tm;      //题名
    private String ssh;     //索书号
    private String cbd;     //出版地
    private String cbz;     //出版者
    private String zrz;     //责任者
    private String cbrq;    //出版日期
    private String bzbm;    //标准编码
    private String cs;      //次数
}
