package com.hdmp.auth.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.hdmp.auth.entity.SysPermissionEntity;
import com.hdmp.auth.entity.SysRoleEntity;
import com.hdmp.auth.entity.SysRolePermissionEntity;
import com.hdmp.auth.entity.SysUserEntity;
import com.hdmp.auth.service.SysRolePermissionService;
import com.hdmp.auth.service.SysRoleService;
import com.hdmp.tool.bean.ResultBean;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author BigFan
 * @create 2018/11/16 4:20 PM
 */
@RestController
@RequestMapping("/role")
public class SysRoleController {
    @Autowired
    SysRoleService sysRoleService;

    @Autowired
    SysRolePermissionService sysRolePermissionService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部角色信息", notes = "获取全部角色信息")
    public ResultBean<List<SysRoleEntity>> findRolesList(@RequestParam String code, @RequestParam String name, int start, int limit) {
        Page<SysRoleEntity> page = new Page<SysRoleEntity>(start, limit);
        Map<String, Object> condition = Maps.newHashMap();
        condition.put("code", code);
        condition.put("name", name);
        sysRoleService.multiQuery(condition,page);
        ResultBean<List<SysRoleEntity>> resultBean = new ResultBean<>();
        resultBean.setData(page.getRecords());
        resultBean.setTotalCount(page.getTotal());
        return resultBean;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation("新增编辑角色信息")
    public ResultBean<SysRoleEntity> save(SysRoleEntity sysRoleEntity) {
        sysRoleService.saveOrUpdate(sysRoleEntity);
        return new ResultBean<>(sysRoleEntity);
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.GET)
    @ApiOperation("根据ID删除角色")
    public ResultBean<SysRoleEntity> delById(@PathVariable Long id ) {
        sysRoleService.removeById(id);
        return new ResultBean<>();

    }

    @RequestMapping(value = "/roleStaffList/{roleId}", method = RequestMethod.GET)
    @ApiOperation("根据角色ID查询分配的用户信息")
    public ResultBean<List<SysUserEntity>> findUserByRoleId(@PathVariable Integer roleId ) {
        return new ResultBean<>(sysRoleService.queryUserByRoleId(roleId));

    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ApiOperation(value = "获取全部角色信息", notes = "获取全部角色信息")
    public ResultBean<List<SysRoleEntity>> findAll() {
        Map<String, Object> condition = Maps.newHashMap();

        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.allEq(condition);
        List<SysRoleEntity> list = sysRoleService.list(queryWrapper);
        ResultBean<List<SysRoleEntity>> resultBean = new ResultBean<>();
        resultBean.setData(list);
        resultBean.setTotalCount(list.size());
        return resultBean;
    }

    @RequestMapping(value = "/rolePermission", method = RequestMethod.GET)
    @ApiOperation("根据角色ID查询所拥有的权限信息")
    public ResultBean<List<SysPermissionEntity>> findPermissionByRoleId(@RequestParam String roleId, @RequestParam String type ) {
        return new ResultBean<>(sysRoleService.findPermissionByRoleId(Integer.parseInt(roleId),type));

    }

    @RequestMapping(value = "/saveRolePermission", method = RequestMethod.POST)
    @ApiOperation("保存角色权限信息")
    public ResultBean<SysRolePermissionEntity> saveRolePermission(@RequestBody SysRolePermissionEntity[] sysRolePermissionEntitys) {
        sysRolePermissionService.saveRolePermission(sysRolePermissionEntitys);
        return new ResultBean<>();
    }
}
