package com.hdmp.auth.service;

import com.google.common.collect.Lists;
import com.hdmp.auth.aspect.DataPermission;
import com.hdmp.auth.entity.SysOrgEntity;
import com.hdmp.auth.entity.SysUserEntity;
import com.hdmp.auth.shiro.ShiroUser;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author BigFan
 * @create 2018/11/18 2:00 PM
 */
@Component
public class DataAuthService {


    @Autowired
    SysOrgService orgService;
    @Autowired
    SysUserService userService;

    /**
     * 生成数据权限相关的sql
     *
     * @param userEntity
     * @param dataPermission
     * @return
     */
    public String createDataSql(SysUserEntity userEntity, DataPermission dataPermission) {
        return createDataSql(userEntity,dataPermission.tableAlias(),dataPermission.sub()
                ,dataPermission.user(),dataPermission.orgField(),dataPermission.userField());

    }

    public String createDataSql() {
        ShiroUser shiroUser = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
        if (shiroUser != null) {
            SysUserEntity userEntity = userService.loadByBdid(shiroUser.getId());
            return createDataSql(userEntity);

        }
        return "";

    }

    public String createDataSql(SysUserEntity sysUserEntity) {

        return createDataSql(sysUserEntity, "", true, true, "org_dbid", "create_by");

    }


    public String createDataSql(SysUserEntity userEntity, String tableAlias,
                                Boolean sub, Boolean userFlag,
                                String orgField, String userField) {

        if(StringUtils.isNotBlank(tableAlias)){
            tableAlias += ".";
        }

        List<Integer> orgList = Lists.newArrayList();

        //用户角色对应表的组织Id
        List<Integer> userOrgList = orgService.queryOrgDbidByUserDbid(userEntity.getDbid());

        orgList.addAll(userOrgList);

        if(sub){
            List<SysOrgEntity> childrens = orgService.queryAllOrg(userEntity.getOrgDbid());
            List<Integer> subList = Lists.newArrayList();
            initAllDbidByTree(subList,childrens);
            subList.add(userEntity.getOrgDbid());
            orgList.addAll(subList);
        }

        StringBuffer sqlBuffer = new StringBuffer("");

        sqlBuffer.append(" (");

        if(orgList.size() >0){
            sqlBuffer.append(tableAlias)
                    .append(orgField)
                    .append(" in (")
                    .append(StringUtils.join(orgList,","))
                    .append(" )");
        }

        sqlBuffer.append(")");
        return sqlBuffer.toString();

    }


    private void initAllDbidByTree(List<Integer> orgIds,List<SysOrgEntity> list){
        list.forEach(e->{
            orgIds.add(e.getDbid());
            List<SysOrgEntity> children = e.getChildren();
            if(children != null && children.size() >0){
                initAllDbidByTree(orgIds,children);
            }
        });

    }
}
