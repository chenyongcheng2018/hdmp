package com.hdmp.wework.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdmp.wework.entity.WechatLogEntity;
import com.hdmp.wework.mapper.WechatLogMapper;
import org.springframework.stereotype.Service;

/**
 * @Author: Chenyc
 * @Date: 2019/1/28 14:12
 */
@Service
public class WechatLogService extends ServiceImpl<WechatLogMapper, WechatLogEntity> {

}