package com.hdmp.wework.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.wework.entity.GryxWechatKeywordsEntity;
import org.apache.ibatis.annotations.Param;

public interface WechatKeywordsMapper extends BaseMapper<GryxWechatKeywordsEntity> {
    GryxWechatKeywordsEntity queryKeywordByAccountidAndKeywords(@Param("accountid") String accountid, @Param("keywords") String keywords);
}