package com.hdmp.mq.config;

import com.hdmp.mq.constant.SysRabbitMqBindConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author BigFan
 * @create 2018/11/21 1:59 PM
 */
@Configuration
public class RabbitMqLogConfigure {

    @Bean
    public DirectExchange sysLogDirectionExchange(){
        System.out.println("init direct....................");
        return new DirectExchange(SysRabbitMqBindConstant.COMMON_LOG_EXCHANGE);
    }

    @Bean
    public Queue sysLogqueue(){
        System.out.println("init queue.....................");

        return new Queue(SysRabbitMqBindConstant.COMMON_LOG_QUEUE);
    }

    @Bean
    public Binding sysLogBinding(){
        return BindingBuilder
                .bind(sysLogqueue())
                .to(sysLogDirectionExchange())
                .with(SysRabbitMqBindConstant.COMMON_LONG_KEY);

    }
}
