package com.hdmp.tool.exception;

import com.hdmp.tool.bean.ResultBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author BigFan
 * @create 2018/11/14 3:41 PM
 */
@RestControllerAdvice
@Slf4j
public class HdmpExceptionHandler {

    @ExceptionHandler(BaseException.class)
    public ResultBean<?> handlerBaseException(BaseException e) {
        ResultBean<String> resultBean = new ResultBean<>();
        resultBean.setCode(e.getCode());
        resultBean.setData(ResultBean.FAIL+"");
        resultBean.setMsg(e.getMsg());
        return resultBean;
    }


    @ExceptionHandler(Exception.class)
    public ResultBean<?> handlerException(Exception e) {
        ResultBean<String> resultBean = new ResultBean<>();
        resultBean.setData(e.getMessage());
        resultBean.setCode(ResultBean.FAIL);
        resultBean.setMsg("exception....");
        log.error(e.getMessage(), e);
        return resultBean;
    }


}
