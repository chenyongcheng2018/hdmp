package com.hdmp.auth.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.hdmp.auth.aspect.SysLog;
import com.hdmp.auth.entity.SysPermissionEntity;
import com.hdmp.auth.entity.SysRoleEntity;
import com.hdmp.auth.entity.SysUserEntity;
import com.hdmp.auth.mapper.SysRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author BigFan
 * @create 2018/11/16 1:15 PM
 */
@Service
public class SysRoleService extends ServiceImpl<SysRoleMapper, SysRoleEntity> {

    @Autowired
    SysRoleMapper roleMapper;


    public Page<SysRoleEntity> multiQuery(Map<String, Object> map, Page<SysRoleEntity> page ){
        page.setRecords(roleMapper.query(page,map));
        return page;
    }

    public List<SysUserEntity> queryUserByRoleId(Integer roleId ){
        return roleMapper.queryUserByRoleId(roleId);
    }

    public List<SysPermissionEntity> findPermissionByRoleId(Integer roleId, String type ){
        return roleMapper.queryPermissionByRoleId(roleId,type);
    }

    /**
     * 根据用户名获取role
     * @param dbid
     * @return
     */
    //@SysLog(title = "查询 userrole")
    public List<SysRoleEntity> queryRoleByUserDbid(Integer dbid){

        List<SysRoleEntity> list = roleMapper.queryRoleByUserDbid(dbid);

        return list == null? Lists.newArrayList():list;


    }
}
