package com.hdmp.libiary.entity;

import lombok.Data;

@Data
public class CurrencyEntity {
    private String txm;//条形码;
    private String dlh;//登录号;
    private String dztm;//读者条码;
    private String wjsj;//外借时间;
    private String yghsj;//应归还时间;
    private String zjm;//主键码;
    private String xjcs;//续借次数;
    private String cjsj;//处理时间;
    private String bz;//备注;
    private String tm;  //书名
    private String ssh; //索书号
    private String zrz; //责任者
    private String cbz; //出版者
    private String bzbm;    //标准编码
    private String cbd; //出版地
    private String cbrq;    //出版日期
    private Boolean sfyxxj; //是否允许续借
    private int hsts;    //剩余还书天数
    private String qfk;
}