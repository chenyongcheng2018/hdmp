package com.hdmp.auth.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import com.hdmp.auth.entity.SysRoleEntity;
import com.hdmp.auth.entity.SysUserEntity;
import com.hdmp.auth.entity.SysUserRoleEntity;
import com.hdmp.auth.mapper.SysRoleMapper;
import com.hdmp.auth.mapper.SysUserRoleMapper;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author: Chenyc
 * @Date: 2019/5/25 15:13
 */
@Service
public class SysUserRoleService extends ServiceImpl<SysUserRoleMapper, SysUserRoleEntity> {
    @Autowired
    SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    SysRoleMapper roleMapper;

    /**
     * 根据用户获取到觉得的dbid
     * @param userEntity
     * @return
     */
    public List<Integer> findRoleDbidByUser(SysUserEntity userEntity) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.apply("user_dbid = " + userEntity.getDbid());
        List<SysUserRoleEntity> list = this.list(queryWrapper);
        list = list == null ? Lists.newArrayList() : list;
        List<Integer> dbids = list.stream().map(e -> e.getRoleDbid()).distinct().collect(Collectors.toList());
        return dbids;
    }

    public List<SysRoleEntity> queryStaffRoles(Integer dbid) {
        return roleMapper.queryStaffRoles(dbid);
    }

    public List<SysRoleEntity> queryUnStaffRoles(Integer dbid) {
        return roleMapper.queryUnStaffRoles(dbid);
    }

    public void deleteUserRoles(Integer userDbid, List<Integer> roleDbid) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("userDbid", userDbid);
        params.put("roleDbid", roleDbid);
        sysUserRoleMapper.deleteByUserIDAndRoleId(params);
    }

}