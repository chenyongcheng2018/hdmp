package com.hdmp.libiary.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hdmp.libiary.entity.CurrencyLogEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CurrencyLogMapper extends BaseMapper<CurrencyLogEntity> {
    List<CurrencyLogEntity> queryByDztm(Page page, @Param("dztm")String dztm,@Param("begtime")String begtime, @Param("endtime")String endtime);
    List<CurrencyLogEntity> queryByDztmAndZjm(@Param("dztm")String dztm, @Param("zjm")String zjm,@Param("begtime")String begtime, @Param("endtime")String endtime);
    int queryTotal(@Param("dztm")String dztm,@Param("begtime")String begtime, @Param("endtime")String endtime);
}