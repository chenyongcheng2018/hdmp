package com.hdmp.auth.service;

import com.hdmp.auth.aspect.SysLog;
import org.springframework.stereotype.Component;

/**
 * @author BigFan
 * @create 2018/11/17 4:33 PM
 */
@Component
public class TestService {


    @SysLog(title = "awwwwwwww")
    public String testAop(){
        System.out.println("execute .....");
        return "success";
    }
}
