package com.hdmp.wework.task;

import com.hdmp.wework.service.WeixinTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class WeworkTokenTask {
    @Autowired
    WeixinTokenService weixinTokenService;

    int count = 0;

    @Scheduled(cron="0 0 0/1 * * ?")
    public void tokenTask(){
        System.out.println("第"+count+"次同步Token信息！！！！");
        weixinTokenService.updateToken();
        count++;
    }
}