package com.hdmp.auth.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author BigFan
 * @create 2018/11/14 8:52 PM
 */
@Data
@TableName("sys_permission")
public class SysPermissionEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键，自增长
     */
    @TableId(type = IdType.AUTO)
    private Integer dbid;

    /**
     * 父级id，一级菜单为0
     */
    private Integer parentId;

    private String name;

    private String code;

    private String url;

    /**
     * 类型(0:目录  1 菜单 2 按钮)
     */
    private String type;

    /**
     * 图标
     */
    private String icon;

    private Integer orders;


    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")//存日期时使用
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")  //取日期时使用
    private Date createTime;

    @TableField(value ="modify_time",fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")//存日期时使用
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")  //取日期时使用
    private Date modifyTime;

    @TableField(value ="create_by",fill = FieldFill.INSERT)
    private Integer createBy;

    @TableField(value="modify_by",fill = FieldFill.UPDATE)
    private Integer modifyBy;

    private Boolean open;

    private String method;

    private Boolean isDisplay;

    private String component;


    @TableField(exist = false)
    private String expid;

    @TableField(exist = false)
    private String expname;

    @TableField(exist = false)
    private List<SysPermissionEntity> children;


}
