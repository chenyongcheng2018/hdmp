package com.hdmp.mq.constant;

/**
 * @author BigFan
 * @create 2018/11/21 2:06 PM
 */
public interface SysRabbitMqBindConstant {

    public final static String COMMON_LOG_EXCHANGE="common_log_exchange";   //通用日志交换器

    public final static String COMMON_LOG_QUEUE = "common_log_queue";

    public final static String COMMON_LONG_KEY ="common_long_key";

}
