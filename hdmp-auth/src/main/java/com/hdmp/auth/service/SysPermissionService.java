package com.hdmp.auth.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.hdmp.auth.entity.SysPermissionEntity;
import com.hdmp.auth.mapper.SysPermissionMapper;
import com.hdmp.tool.util.LambdaTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import java.util.stream.Collectors;

/**
 * @author BigFan
 * @create 2018/11/16 12:40 PM
 */
@Service
public class SysPermissionService extends ServiceImpl<SysPermissionMapper,SysPermissionEntity> {

    @Autowired
    SysPermissionMapper permissionMapper;

    public List<SysPermissionEntity> findByType(String type){
        return permissionMapper.queryPermissionByType(type);
    }

    public List<SysPermissionEntity> findByParentId(String parentId, String type){
        return permissionMapper.queryByParentIdAndType(Integer.parseInt(parentId), type);
    }

    @Transactional
    public boolean deletePermission(String id) {
        Long temp = Long.parseLong(id);
        try{
            //删除权限
            permissionMapper.deleteById(temp);
            //删除其对应的权限表达式数据
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 以组织数的形式返回
     *
     * @param parentMenu
     * @return
     */
    public List<SysPermissionEntity> getAllChildrenMenu(Integer parentMenu) {

        List<SysPermissionEntity> allMenus = this.list(null);

        List<SysPermissionEntity> rootList = allMenus.stream().filter(e->e.getParentId() == parentMenu).filter(e->"00".equals(e.getType()))
                .sorted(Comparator.comparingInt(SysPermissionEntity::getOrders)).collect(Collectors.toList());
        if(rootList != null && rootList.size()>0){
            rootList.forEach(e->initMenuTreeList(allMenus,e));
        }
        return rootList;

    }

    /**
     *递归调用
     */
    public void initMenuTreeList(List<SysPermissionEntity> allList,SysPermissionEntity parent){
        List<SysPermissionEntity> children = allList.stream()
                .filter(e->e.getParentId() == parent.getDbid()).filter(e->"00".equals(e.getType()))
                .sorted(Comparator.comparingInt(SysPermissionEntity::getOrders))
                .collect(Collectors.toList());
        if(children != null && children.size()>0){
            parent.setChildren(children);
            children.forEach(e->initMenuTreeList(allList,e));
        }
    }
    /**
     * 获取用拥有的menu
     *
     * @param userId
     * @return
     */
    public List<SysPermissionEntity> queryMenuByUserId(Integer userId) {
        List<SysPermissionEntity> list = permissionMapper.queryMenuUserDbid(userId);
        list = list == null? Lists.newArrayList():list;
        LambdaTool lambdaTool = new LambdaTool();

        List<SysPermissionEntity> distinctList = list.stream()
                .filter(lambdaTool.distinctByKey(SysPermissionEntity::getDbid))
                .collect(Collectors.toList());

        return distinctList;

    }

}
