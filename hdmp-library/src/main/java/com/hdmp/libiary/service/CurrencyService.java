package com.hdmp.libiary.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdmp.libiary.entity.CurrencyEntity;
import com.hdmp.libiary.mapper.CurrencyMapper;
import com.hdmp.mybatis.aspect.annotation.HdDataSource;
import com.hdmp.mybatis.constant.DataSourceEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class CurrencyService extends ServiceImpl<CurrencyMapper, CurrencyEntity> {
    @Autowired
    CurrencyMapper currencyMapper;

    @HdDataSource(DataSourceEnum.SLAVE)
    public List<CurrencyEntity> queryByDztm(String dztm){
        return currencyMapper.queryByDztm(dztm);
    }

    @HdDataSource(DataSourceEnum.SLAVE)
    public CurrencyEntity queryByDztmAndZjm(String dztm, String zjm){
        return currencyMapper.queryByDztmAndZjm(dztm, zjm);
    }

    @HdDataSource(DataSourceEnum.SLAVE)
    public String renew(CurrencyEntity entity) {
        System.out.println(entity);
        System.out.println("*********************");
        String error = "";
        if("0".equals(entity.getXjcs())){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try{
                Date date1 = sdf.parse(entity.getYghsj());
                System.out.println(entity.getYghsj());
                System.out.println("~~~~~~~~~~~~~~~原归还时间");
                Calendar cal = Calendar.getInstance();
                cal.setTime(date1);
                cal.add(Calendar.DAY_OF_YEAR,20);
                entity.setYghsj(sdf.format(cal.getTime()));
                entity.setXjcs("1");
                System.out.println(entity.getYghsj());
                System.out.println("……………………………………………………………………后归还时间");
                //执行保存操作
                int i = currencyMapper.updateByDztmAndZjm(entity.getYghsj(), entity.getDztm(), entity.getZjm());
                if(i > 0){
                    return "续借成功";
                }else{
                    return "系统忙碌，请稍后再试";
                }

            }catch (Exception e){
                e.printStackTrace();
                return"服务器忙，请稍后再试";
            }
        }else{
            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            System.out.println("无续借次数");
            return "无续借次数";
        }
    }
}