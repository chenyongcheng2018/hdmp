package com.hdmp.libiary.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.libiary.entity.ReaderEntity;
import org.apache.ibatis.annotations.Param;

public interface ReaderMapper extends BaseMapper<ReaderEntity> {
    ReaderEntity queryByUsername(@Param("username") String username);
}