package com.hdmp.libiary.entity;

import lombok.Data;

@Data
public class BooksEntity {
    private int kjm;  //库键码
    private String zjm;  //主键码
    private String tm;// 题名;
    private String tmsx;//题名缩写;
    private String yz;//语种;
    private String bc;//版次;
    private String zrz;//责任者;
    private String cbz;//出版者;
    private String cbd;//出版地;
    private String cbrq;//出版日期;
    private String bzbm;//标准编码;
    private String ssh;//索书号;
    private int cs;//册数;
    private int kwjs;//可外借数;
    private int ywjs;//已外借数;
    private int yys;//预约数;
    private int jg;//价格;
}