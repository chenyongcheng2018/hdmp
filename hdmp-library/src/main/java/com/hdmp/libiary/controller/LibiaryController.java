package com.hdmp.libiary.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hdmp.auth.aspect.LogType;
import com.hdmp.auth.aspect.SysLog;
import com.hdmp.auth.entity.SysUserEntity;
import com.hdmp.auth.shiro.ShiroUser;
import com.hdmp.libiary.entity.BooksEntity;
import com.hdmp.libiary.entity.CurrencyEntity;
import com.hdmp.libiary.entity.CurrencyLogEntity;
import com.hdmp.libiary.service.BooksService;
import com.hdmp.libiary.service.CurrencyLogService;
import com.hdmp.libiary.service.CurrencyService;
import com.hdmp.libiary.service.LibiaryService;
import com.hdmp.tool.bean.ResultBean;
import com.hdmp.wework.service.WeixinTokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/libiary")
@Api(description = "libiary统一入口")
@CrossOrigin
public class LibiaryController {

    @Autowired
    LibiaryService libiaryService;
    @Autowired
    WeixinTokenService weixinTokenService;
    @Autowired
    BooksService booksService;
    @Autowired
    CurrencyService currencyService;
    @Autowired
    CurrencyLogService logService;

    @GetMapping("/test")
    public String test(){
        System.out.println("0000000000000000000000000000000000000000000000000000000000000000000000000");
        return "hel;loe test";
    }

    @PostMapping(value = "/binding")
    @ApiOperation(value = "绑定卡号", notes = "微信绑定卡号")
    @SysLog(title = "绑定卡号",logType = LogType.UPDATE)
    public ResultBean<?> binding(@RequestParam(name = "username")String username,
                                 @RequestParam(name = "password")String password){
        ShiroUser shiroUser = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
        return libiaryService.bindCode(username, password,shiroUser.getUsername());
    }

    @PostMapping(value = "/rebinding")
    @ApiOperation(value = "解除绑定卡号", notes = "微信解除绑定卡号")
    @SysLog(title = "解除绑定卡号",logType = LogType.UPDATE)
    public ResultBean<?> reBinding(){
        ShiroUser shiroUser = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
        return libiaryService.rebindCode(shiroUser.getUsername());
    }

    //查询用户
    @GetMapping(value = "/code")
    @ApiOperation(value = "查询用户", notes = "查询用户判断是否绑定卡号")
    public ResultBean<?> getUser(){
        ShiroUser shiroUser = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
        SysUserEntity userEntity = libiaryService.getUser(shiroUser.getUsername());
        return new ResultBean<>(userEntity);
    }

    @GetMapping(value = "/Jssdk")
    @ApiOperation(value = "查询用户", notes = "查询用户判断是否绑定卡号")
    public ResultBean<?> getJssdk(@RequestParam("appId") String appId, @RequestParam("url") String url){
        return new ResultBean<>(weixinTokenService.getJssdk(appId, url));
    }

    @GetMapping(value="/queryBooks")
    @SysLog(title = "扫码查询功能",logType = LogType.SELECT)
    public ResultBean<?> queryByTm(@RequestParam(value="pageNum")int pageNum,
                                       @RequestParam(value="num")int num,
                                       @RequestParam(value="tm")String tm,
                                       @RequestParam(value="type")String type){
        System.out.println("tm="+tm.toUpperCase());
        tm = tm.toUpperCase();
        if("sm".equals(type)){
            tm = "题名 like '%"+tm+"%'";
        }else if("zr".equals(type)){
            tm = "责任者 like '%"+tm+"%'";
        }else if("cb".equals(type)){
            tm = "出版者 like '%"+tm+"%'";
        }else if("bm".equals(type)){
            tm = "标准编码 like '%"+tm+"%'";
        }else if("ewm".equals(type)){
            tm = "(标准编码 = '"+libiaryService.toisbn(tm)+"' or 标准编码 = '"+libiaryService.to13isbn(tm)+"')";
        }else{
            return new ResultBean<>();
        }
        Page<BooksEntity> page = new Page<>(pageNum, num);
        return new ResultBean<>(booksService.queryByTm(page, tm));
    }

    @GetMapping(value="/queryCur")
    @SysLog(title = "查询我的现有借书",logType = LogType.SELECT)
    public List<CurrencyEntity> queryByTm(@RequestParam(value="dztm")String dztm){
        System.out.println("queryCur::dztm="+dztm);
        return libiaryService.queryCurrency(dztm);
    }

    @PostMapping(value="/renew")
    //续借
    @SysLog(title = "续借操作",logType = LogType.UPDATE)
    public String  renew(@RequestParam(value="dztm")String dztm, @RequestParam(value="zjm")String zjm){
        CurrencyEntity currencyEntity = currencyService.queryByDztmAndZjm(dztm, zjm);
        return currencyService.renew(currencyEntity);
    }

    @GetMapping(value="/queryRecords")
    @SysLog(title = "查询借阅历史",logType = LogType.SELECT)
    public ResultBean queryRecords(@RequestParam(value="pageNum")int pageNum,
                                   @RequestParam(value="num")int num,
                                   @RequestParam(value="dztm")String dztm,
                                   @RequestParam(value="begtime")String begtime,
                                   @RequestParam(value="endtime")String endtime){
        System.out.println(pageNum+":::"+num+":::"+dztm);
        Page<CurrencyLogEntity> page = new Page<>(pageNum, num);
        return new ResultBean<>(logService.queryByDztm(page, dztm, begtime, endtime));
    }

    @PostMapping(value="/queryRecordDetail")
    @SysLog(title = "查询借阅历史明细",logType = LogType.SELECT)
    public ResultBean queryRecordDetail(@RequestParam(value="dztm")String dztm,
                                        @RequestParam(value="zjm")String zjm,
                                        @RequestParam(value="begtime")String begtime,
                                        @RequestParam(value="endtime")String endtime){
        return new ResultBean<>(logService.queryByDztmAndZjm(dztm, zjm, begtime, endtime));
    }
}