package com.hdmp.mybatis.constant;

/**
 * @author BigFan
 * @create 2018/11/20 12:56 PM
 */
public enum DataSourceEnum {
    MASTER("master"),SLAVE("slave"),OTHER("other");

    private String value;

    DataSourceEnum(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
