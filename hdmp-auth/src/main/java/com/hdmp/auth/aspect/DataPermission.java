package com.hdmp.auth.aspect;

import com.hdmp.mybatis.constant.DatabaseFieldConstant;

import java.lang.annotation.*;

/**
 * @author BigFan
 * @create 2018/11/18 1:17 PM
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataPermission {

    /**
     * 表别名
     */
    String tableAlias() default "";

    /**
     * 是否能查看个人数据
     * @return
     */
    boolean user() default true;

    /**
     * 查看下级数据
     * @return
     */
    boolean sub() default false;

    /**
     * 部门字段
     * @return
     */
    String orgField() default "org_dbid";

    /**
     * 用户字段
     * @return
     */
    String userField() default DatabaseFieldConstant.CREATE_BY;



}
