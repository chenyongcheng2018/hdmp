package com.hdmp.libiary.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdmp.libiary.entity.CurrencyEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CurrencyMapper extends BaseMapper<CurrencyEntity> {
    List<CurrencyEntity> queryByDztm(@Param("dztm") String dztm);
    CurrencyEntity queryByDztmAndZjm( @Param("dztm")String dztm, @Param("zjm")String zjm);
    int updateByDztmAndZjm(@Param("yghsj")String yghsj, @Param("dztm")String dztm, @Param("zjm")String zjm);
}