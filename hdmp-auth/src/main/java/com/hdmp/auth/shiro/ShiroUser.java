package com.hdmp.auth.shiro;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

/**
 * @author BigFan
 * @create 2018/11/14 7:41 PM
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShiroUser implements Serializable {
    private Integer id;
    private String username;
    private String name;

    private Set<String> roles;
    private Set<String> urls;
    private Set<String> perms;


}
